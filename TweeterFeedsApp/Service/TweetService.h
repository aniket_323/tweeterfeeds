//
//  TweetService.h
//  TweeterFeedsApp
//
//  Created by Anup Gondane on 22/04/15.
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TweetService : NSObject<NSURLSessionDataDelegate,NSURLSessionTaskDelegate,NSURLSessionDelegate,NSURLSessionDownloadDelegate>

-(void)getDataFromServiceWithSuccessBlock:(void(^)(NSArray *responseObject))success failureBlock:(void(^)(NSError *error))failure;

@end
