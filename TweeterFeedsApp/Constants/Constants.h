//
//  constants.h
//  MyHomeShop
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TABLE_ROW_HEIGHT 50
#define ROW_LABEL_WIDTH   200
#define ROW_LABEL_HEIGHT 20
#define ROW_TITLE_TAG    500
#define SIMPLE_FONT_12   [UIFont systemFontOfSize:12]

#define NAVBAR_COLOR [UIColor colorWithRed:(76.0f/255.0f) green:(221.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f]

#define SERVICE_URL  @"https://alpha-api.app.net/stream/0/posts/stream/global"


#define KEY_USERNAME @"username"
#define KEY_URL @"url"