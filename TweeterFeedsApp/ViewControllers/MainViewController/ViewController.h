//
//  ViewController.h
//  TweeterFeedsApp
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,retain) NSArray *dataArray;
@end

