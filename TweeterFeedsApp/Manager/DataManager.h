//
//  DataManager.h
//  TweeterFeedsApp
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

-(void)getDataFromServiceWithSuccessBlock:(void(^)(NSArray *responseObject))success failureBlock:(void(^)(NSError *error))failure;

@end
