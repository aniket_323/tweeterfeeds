//
//  TweetsParser.h
//  TweeterFeedsApp
//
//  Created by Anup Gondane on 23/04/15.
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TweetsParser : NSObject
+(NSMutableArray *)getFeedsFromData:(NSDictionary *)jsonDictionary;
@end
