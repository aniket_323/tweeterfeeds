//
//  TweetService.m
//  TweeterFeedsApp
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import "TweetService.h"
#import "TweetsParser.h"
#import "Constants.h"
@implementation TweetService
{
    NSString *serviceUrl;
    NSURLSession *session;
    NSDictionary *jsonDic;
}

-(id)init
{
    self= [super init];
    if(self)
    {
        [self initalizeHeaders];
    }
    return self;
}

-(void)initalizeHeaders{
    
    serviceUrl = [NSString stringWithFormat:@"%@",SERVICE_URL];
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
   
}

-(void)getDataFromServiceWithSuccessBlock:(void(^)(NSArray *responseObject))success failureBlock:(void(^)(NSError *error))failure
{

    __block  NSArray *feeds = [NSArray array];
    
    NSURL *downloadTaskURL = [NSURL URLWithString:[serviceUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *req = [[NSURLRequest alloc]initWithURL:downloadTaskURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0];
    
    [[session dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSDictionary *feedsDic = [NSJSONSerialization
                               JSONObjectWithData:data
                               options:kNilOptions
                               error:&error];
        
        //try block
        @try {
          
            if ([NSJSONSerialization isValidJSONObject:feedsDic]) {
                
                feeds  = [TweetsParser getFeedsFromData:feedsDic];
                if (feeds) {
                    success(feeds);

                }
                else{
                    
                }
                
                
            }
            
            else{
                
                NSLog(@"Not a valid JSON");
                failure(error);
            }

            
        }
       
        @catch (NSException *exception) {
            
            NSLog(@"Exception %@",exception);
            failure(error);
        }
        
       
        
    }] resume];
    



}

@end
