 //
//  ViewController.m
//  TweeterFeedsApp
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import  "DataManager.h"
#define CELL_IDENTIFIER @"CellIdentifier"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any addit/Users/anup/Desktop/TweeterFeedsApp/TweeterFeedsApp/ViewController.mional setup after loading the view, typically from a nib.
    self.tableView.dataSource = (id)self;
    self.tableView.delegate = (id)self;
    [self serviceCall];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview datasource

// Default is 1 if not implemented
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
    return  [self.dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell =nil;
    static NSString * cellIdentifier = CELL_IDENTIFIER;
    
    //recharge tableview
    if (tableView == self.tableView) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil)
        {
            cell = [self tableViewCellWithReuseIdentifier:cellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        [self configureCell:cell indexPath:indexPath];
        
    }
    
    return cell;
}


#pragma mark table helpers
-(UITableViewCell*)tableViewCellWithReuseIdentifier:(NSString*)cellIdentifier
{
    UITableViewCell * cell = nil;
    if ([cellIdentifier isEqualToString:CELL_IDENTIFIER])
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        int xPos = 10;
        int yPos = 20;
        UILabel *tweetDescriptionLbl = [[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, ROW_LABEL_WIDTH, ROW_LABEL_HEIGHT)];
        tweetDescriptionLbl.textColor = [UIColor blackColor];
        tweetDescriptionLbl.font = SIMPLE_FONT_12;
        tweetDescriptionLbl.tag = ROW_TITLE_TAG;
        [cell.contentView addSubview:tweetDescriptionLbl];
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

-(void)configureCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath
{
    UILabel *tweetDescriptionLbl = (UILabel *)[cell.contentView viewWithTag:ROW_TITLE_TAG];
    NSDictionary *dictionary = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nameStr = [dictionary valueForKey:KEY_USERNAME];
    tweetDescriptionLbl.text  = nameStr;
    
}

#pragma mark call on backgroundThread
-(void)serviceCall{
    
    DataManager *dataManager = [[DataManager alloc]init];
    
    [dataManager getDataFromServiceWithSuccessBlock:^(NSArray *responseObject) {
        
        [self performAfterDataLoad:responseObject];
       
    } failureBlock:^(NSError *error) {
        
        NSLog(@"Error");
        
    }];
    
}

-(void)performAfterDataLoad:(NSArray *)responseObject{
    
    self.dataArray = nil;
    
    if (responseObject.count>0) {
        
        self.dataArray = [[NSArray alloc]initWithArray:responseObject];
        [self.tableView reloadData];

    }

}


@end
