//
//  TweetsParser.m
//  TweeterFeedsApp
//
//  Copyright (c) 2015 aniket. All rights reserved.
//

#import "TweetsParser.h"
#import "Constants.h"
@implementation TweetsParser

+(NSMutableArray *)getFeedsFromData:(NSDictionary *)jsonDictionary{
   
    NSMutableArray *feeds = [NSMutableArray array];
    
    NSArray *rootDataArray = [jsonDictionary valueForKey:@"data"];
    for (NSDictionary *dataDic in rootDataArray) {
        NSDictionary *userDic = [dataDic valueForKey:@"user"];
        if ([userDic valueForKey:KEY_USERNAME] && [[userDic valueForKey:KEY_USERNAME] length]>0) {
            
            NSDictionary *avtarDic = [userDic valueForKey:@"avatar_image"];
            if ([avtarDic valueForKey:KEY_URL] && [[avtarDic valueForKey:KEY_URL] length]>0) {
               
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                [dic setObject:[avtarDic valueForKey:KEY_URL] forKey:KEY_URL];
                [dic setObject:[userDic valueForKey:KEY_USERNAME] forKey:KEY_USERNAME];
                [feeds addObject:dic];
            }
        }
    }
    
    return feeds;
}

@end
