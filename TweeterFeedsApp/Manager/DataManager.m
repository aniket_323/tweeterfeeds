//  DataManager.m
//  TweeterFeedsApp
//  Copyright (c) 2015 aniket. All rights reserved.

#import "DataManager.h"
#import "TweetService.h"
@implementation DataManager

-(id)init
{
    self = [super init];
    if(self)
    {
    
    }
    return self;
}

-(void)getDataFromServiceWithSuccessBlock:(void(^)(NSArray *responseObject))success failureBlock:(void(^)(NSError *error))failure
{
   
    //Service call to TweetService
    TweetService *objTweetService = [[TweetService alloc]init];
    
    [objTweetService getDataFromServiceWithSuccessBlock:^(NSArray *responseObject) {
        success(responseObject);
        
    } failureBlock:^(NSError *error) {
        failure(error);
    }];
        
     
    
}




@end